import log from "@ajar/marker";

function random(): number;
function random(x: true): number;
function random(x: true, y: number): number;
function random(x: true, y: number, z: number): number;

function random(x: false, y: number): number;
function random(x: false, y: number, z: number): number;

function random(x?: boolean, y?: number, z?: number): number {
    if (typeof x === "boolean") {
        if (x === true) {
            if (typeof y === "number") {
                if (typeof z === "number") {
                    return y + (Math.random()*(z-y));//flaot
                }
                return Math.random()*y;//flaot
            }
            else {
                return Math.random();//flaot
            }
        }
        else {
            if (typeof y === "number") {
                if (typeof y === "number") {
                    return Math.round(y + (Math.random()*(z-y)));//integer
                }
                return Math.round(Math.random()*y);//integer
            }
        }
    }
    return Math.round(Math.random());//integer
}


const num1 = random(); // 0 ~ 1 | integer
const num2 = random(true); // 0 ~ 1 | float
const num5 = random(true, 6); // 0 ~ 6 | float /
const num6 = random(true, 2, 6); // 2 ~ 6 | float/

const num3 = random(false, 6); // 0 ~ 6 | integer,
const num4 = random(false, 2, 6); // 2 ~ 6 | integer

console.log({ num1, num2, num3, num4, num5, num6 });